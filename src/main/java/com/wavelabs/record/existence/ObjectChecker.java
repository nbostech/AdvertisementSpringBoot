package com.wavelabs.record.existence;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.User;
import com.wavelabs.repo.UserRepo;

@Component
public class ObjectChecker {

	private static final Logger logger = Logger.getLogger(ObjectChecker.class);

	Session session = null;

	@Autowired
	private UserRepo userRepo;

	public boolean isUserExist(String uuid, String tenantId) {
		try {
			User user = userRepo.findByUuidAndTenantId(uuid, tenantId);
			return (user != null);
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
