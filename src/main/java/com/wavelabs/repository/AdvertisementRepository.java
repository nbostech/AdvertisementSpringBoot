package com.wavelabs.repository;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.wavelabs.model.Advertisement;
import com.wavelabs.model.User;
import com.wavelabs.repo.AdvertisementRepo;
import com.wavelabs.repo.UserRepo;

@Component
public class AdvertisementRepository {

	private static final Logger LOGGER = Logger.getLogger(AdvertisementRepository.class);

	@Autowired
	private AdvertisementRepo advertisementRepo;

	@Autowired
	private UserRepo userRepo;

	public Advertisement getAdvertisement(int id) {

		return advertisementRepo.findById(id);
	}

	public boolean persistAdvertisement(Advertisement add) {
		try {
			advertisementRepo.save(add);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	public Advertisement updateAdvertisement(Advertisement add) {

		try {
			advertisementRepo.save(add);
			return add;
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public boolean deleteAdvertisement(int id) {
		try {

			advertisementRepo.delete(id);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Advertisement> getAllAdvertisementsOfUser(int id) {
		try {
			User user = userRepo.findById(id);
			List<Advertisement> list = advertisementRepo.findAllByUser(user);
			return list;
		} catch (Exception e) {
			LOGGER.error(e);
			return Collections.emptyList();
		}
	}
}
