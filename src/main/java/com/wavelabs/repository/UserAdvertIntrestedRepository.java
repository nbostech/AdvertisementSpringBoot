package com.wavelabs.repository;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.User;
import com.wavelabs.model.UserAdvertIntrested;
import com.wavelabs.repo.UserAdvertIntrestedRepo;
import com.wavelabs.repo.UserRepo;

@Component
public class UserAdvertIntrestedRepository {

	private static final Logger LOGGER = Logger.getLogger(UserAdvertIntrestedRepository.class);

	@Autowired
	private UserAdvertIntrestedRepo userAdvert;

	@Autowired
	private UserRepo userRepo;

	public boolean persistUserAdverIntrested(UserAdvertIntrested uai) {

		try {
			userAdvert.save(uai);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	public UserAdvertIntrested get(int id) {
		try {
			return userAdvert.findOne(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<UserAdvertIntrested> getUserAdvertIntrested(int id) {
		User user = userRepo.findOne(id);
		List<UserAdvertIntrested> listOfAdverts = userAdvert.findAllByUser(user);
		return listOfAdverts;
	}

}
