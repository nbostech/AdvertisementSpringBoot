package com.wavelabs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.model.UserAdvertIntrested;
import com.wavelabs.repository.UserAdvertIntrestedRepository;

@Service
public class UserAdvertIntrestService {

	@Autowired
	private UserAdvertIntrestedRepository uao;

	public UserAdvertIntrestService(UserAdvertIntrestedRepository uao) {

		this.uao = uao;
	}

	public boolean persistUserAdvert(UserAdvertIntrested uia) {
 
		return uao.persistUserAdverIntrested(uia);
	}

	public UserAdvertIntrested getUserAdvert(int id) {

		return uao.get(id);
	}

	public List<UserAdvertIntrested> getAllUserAdvertIntrests(int id) {

		return uao.getUserAdvertIntrested(id);
	}
}
