package com.wavelabs.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.User;

public interface UserRepo extends CrudRepository<User, Integer> {

	
	public User findById(Integer id);
	
	public User findByUuidAndTenantId(String uuid, String tenantId);
	
}
