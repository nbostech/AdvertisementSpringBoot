package com.wavelabs.repo;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.Advertisement;
import com.wavelabs.model.Comment;

public interface CommentRepo extends CrudRepository<Comment, Integer> {

	
	
	public Comment[] findAllByAdvertisement(Advertisement add);
	
	
	
}
