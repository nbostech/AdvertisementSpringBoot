package com.wavelabs.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.Advertisement;
import com.wavelabs.model.User;

public interface AdvertisementRepo extends CrudRepository<Advertisement, Integer> {


	public Advertisement findById(Integer id);

	public List<Advertisement> findAllByUser(User user);


}
