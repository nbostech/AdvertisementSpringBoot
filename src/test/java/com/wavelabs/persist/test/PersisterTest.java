package com.wavelabs.persist.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.model.User;
import com.wavelabs.persist.Persister;
import com.wavelabs.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class PersisterTest {

	@Mock
	private UserService userService;

	@InjectMocks
	private Persister persister;

	@Test
	public void testPersistUser() {
		when(userService.createUser(any(User.class))).thenReturn(true);
		boolean flag = persister.persistUser(mock(User.class));
		Assert.assertEquals(true, flag);
	}
}
