package com.wavelabs.model.test;

import java.util.Calendar;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Comment;

@RunWith(MockitoJUnitRunner.class)
public class CommentTest {

	@InjectMocks
	private Comment commentMock;

	private Comment comment;

	@Before
	public void setUp() {

		comment = ObjectBuilder.getComment();

	}

	@Test
	public void testgetId() {
		commentMock.setId(comment.getId());
		Assert.assertEquals(comment.getId(), commentMock.getId());
	}

	@Test
	public void testGetAdvert() {
		commentMock.setAdvertisement(comment.getAdvertisement());
		Assert.assertEquals(comment.getAdvertisement().getLocation(), commentMock.getAdvertisement().getLocation());

	}

	@Test
	public void testGetUser() {

		commentMock.setUser(comment.getUser());
		Assert.assertEquals(comment.getUser().getTenantId(), commentMock.getUser().getTenantId());
	}

	@Test
	public void testGetText() {
		commentMock.setText(comment.getText());
		Assert.assertEquals(comment.getText(), commentMock.getText());
	}

/*	@Test
	public void testGetTimeStamp() {

		commentMock.setTimeStamp(Calendar.getInstance());
		Assert.assertEquals(comment.getTimeStamp(), commentMock.getTimeStamp());
	}
*/
}
