package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Member;

@RunWith(MockitoJUnitRunner.class)
public class MemberTest {

	
	
	@Test
	public void testGetId() {
		
		Member member = ObjectBuilder.getMember();
		Member memberTest = new Member();
		memberTest.setUuid(member.getUuid());
		Assert.assertEquals(member.getUuid(), memberTest.getUuid());
	}
}
