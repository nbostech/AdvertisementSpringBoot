package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Modules;

@RunWith(MockitoJUnitRunner.class)
public class ModulesTest {

	@InjectMocks
	private Modules modulesMock;

	private Modules modules;

	@Before
	public void setUp() {

		modules = ObjectBuilder.getModule();
	}

	@Test
	public void testGetName() {

		modulesMock.setName(modules.getName());
		Assert.assertEquals(modules.getName(), modulesMock.getName());
	}

	@Test
	public void testGetUuid() {

		modulesMock.setUuid(modules.getUuid());
		Assert.assertEquals(modules.getUuid(), modulesMock.getUuid());

	}

}
